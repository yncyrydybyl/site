---
title: "Get in contact"
linktitle: "Get in contact"
---


- **Address:** Rue de la Petite Île 1, 1070 Anderlecht, Brussels, Belgium.
- **Pidgeon:** 50.832593, 4.322160
    ([GoogleMaps](https://goo.gl/maps/NwCELYFNmXp))([OpenStreetMap](https://www.openstreetmap.org/search?query=hackerspace%20brussels#map=19/50.83233/4.32128))
- **Email:** [contact@hsbxl.be](mailto:contact@hsbxl.be)
- **Telephone:** [+32 2 880 40 04](tel:003228804004) (phone is in space)
- **EventPhone VPN Telephone:** [1070](tel:1070) (https://eventphone.de/doku/epvpn)
- **Facebook:** [HSBXL on Facebook](https://www.facebook.com/groups/hsbxl/)
- **Twitter:** [hsbxl](http://twitter.com/hsbxl) and [\#hsbxl](https://twitter.com/search?q=%23hsbxl)
- **Mastodon:** [@hsbxl@mastodon.social](https://mastodon.social/@hsbxl)
- **Matrix chat:** \#hsbxl on hackerspaces.be ([webchat](https://matrix.to/#/#hsbxl:matrix.org))
- **IRC:** \#hsbxl on irc.libera.chat ([webchat](https://matrix.to/#/#hsbxl:matrix.org))
- **Mailing-list:** see the [ mailing-List](/mailinglist) page
- **Bank:** Argenta IBAN: BE69 9794 2977 5578; BIC: ARSP BE 22

# How to get there?

Call us if the gate is closed - telephone above

## By public transport 🚆

From Brussels South Train Station:  
The space is located at 950m from Brussels south transtation [Route on Google maps](https://goo.gl/maps/MzAzVSRpjB72)

From Brussels South Train Station you can also take a bus that stops near the hackerspace:
* [STIB bus 78](https://m.stib.be/line.php?lang=en&line=78&iti=1) ( stop "Deux Gares" ) - 200m walk

## By car 🚘
Most of the time, there are free parking spots in Rue des Goujons. At the front gate (Rue de la petite île), parking is paying since the summer of 2022.

## Shared mobility
Brussels has a [vast offering in shared mobility](https://www.brussels.be/alternative-mobility). Use a bike, step or even car using the appropriate plans.
## At night: Collecto

**NOTICE: This service is suspended until further notice. Officially due to COVID19-measures. Check [the website](https://taxisverts.be/en/collecto-en/) for updates.**

To get back at your home/hotel, there is 'Collecto'. A Taxi picks you up at certain points and brings you to your destination within the Brussels region for 6 euro (5 if you have a STIB subscription). Fixed price.


You can order a Collecto at any time between 11 p.m. and 6 a.m, but it is advisable to order at least 20 minutes in advance. If you do not have a smartphone to use the application ([Android](https://play.google.com/store/apps/details?id=be.tradecom.collecto&hl=en&gl=US) - [Apple](You can order a Collecto at any time between 11 p.m. and 6 a.m, but it is advisable to order at least 20 minutes in advance. If you do not have a smartphone to use the application, you can also call 02/800 36 36 to reserve your Collecto.)), you can also call 02/800 36 36 to reserve your Collecto.

There is a [collecto](https://taxisverts.be/en/collecto-en/) startpoint around the corner, at 350m. see https://goo.gl/maps/YiHmCjo8EZ62  

# Statutes at Belgisch Staatsblad / Moniteur Belge
- **Trade number**: BE0817617057
- **Official link to all statutes for HSBXL**: [website FOD Justice](http://www.ejustice.just.fgov.be/cgi_tsv/tsv_rech.pl?language=nl&btw=0817617057&liste=Liste)
- **2009-08-06**: [Statutes: Rubric Constitution (New Juridical Person, Opening Branch, etc...)](https://www.ejustice.just.fgov.be/tsv_pdf/2009/08/18/09118276.pdf)
- **2013-08-12**: [Statutes: Designation - Registered Office - Resignations - Appointments](https://www.ejustice.just.fgov.be/tsv_pdf/2013/08/22/13131033.pdf)
- **2016-05-10**: [Statutes: Registered Office - Resignations - Appointments](https://www.ejustice.just.fgov.be/tsv_pdf/2016/05/20/16069254.pdf)
- **2019-07-17**: [Statutes: Registered Office - Resignations - Appointments](https://www.ejustice.just.fgov.be/tsv_pdf/2019/07/24/19099868.pdf)
- **2022-01-10**: [Statutes: Resignations - Appointments](http://www.ejustice.just.fgov.be/tsv_pdf/2022/01/17/22007203.pdf)
