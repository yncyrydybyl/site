---
title: "HSBXL OIDs"
linktitle: "OID"
---

/!\ This page will need some rework.

Is there a way to automatically generate a MIB using a YAML file ?

# PEN/OID

We have the following PEN from IANA: 1.3.6.1.4.1.43666.

This is based on previous work by TQ_Hirsh

## 1.3.6.1.4.1.43666.1 (EXPERIMENTAL)

## 1.3.6.1.4.1.43666.2 (LDAP)

### 1.3.6.1.4.1.43666.2.1 (LDAP Attribute type)

1.  x-hsbxl-pgpKey - PGP public key used for encrypted communications
2.  x-hsbxl-sponsorID - Sponsoring member
3.  x-hsbxl-membershipReason - Why you're becoming a member
4.  x-hsbxl-sshPubKey - SSH public key

The following entries might be included in the definitive user schema

1.  x-hsbxl-membershipStructuredComm - Structured communication for
    membership payments
2.  x-hsbxl-membershipAccountId - Internal identifier for membership
    account
3.  x-hsbxl-drinksStructuredcomm - Structured communication for drink
    account
4.  x-hsbxl-drinksAccountId - Internal identifier for fridge account
5.  x-hsbxl-RFID-id - Access control token ID

### 1.3.6.1.4.1.43666.2.2 (LDAP Object class)

## 1.3.6.1.4.1.43666.3 (SNMP)

(todo)

## 1.3.6.1.4.1.43666.4 (Member-defined objects)

This arc is reserved for HSBXL member-defined attributes. To use it, get
your uidNumber from our LDAP database, and append it to this OID.
Anything defined below the resulting OID is up to you.

    Example:
    
    Your LDAP uidNumber is 4242. Your personal arc will then be 1.3.6.1.4.1.43666.4.4242.
    
    Your 1.3.6.1.4.1.43666.4.4242 arc can then be subdivided as you like:
     1.3.6.1.4.1.43666.4.4242.1 (arc foo)
     1.3.6.1.4.1.43666.4.4242.2 (arc bar)
     1.3.6.1.4.1.43666.4.4242.3 (arc baz)

