---
startdate:  2021-09-26
starttime: "14:00"
endtime: "17:30"
linktitle: "Adversarial Noise"
title: "Adversarial Noise"
price: "tip jar"
series: "adversarial-noise"
eventtype: "Workshop"
location: "HSBXL"
image: "javierlloret.jpeg"
hackeragenda: "true"
---

## Topic
Javier Lloret is a researcher, media artist and engineer based in Amsterdam. He is conducting research on the vulnerabilities of AI-based Computer Vision systems to Adversarial Attacks. 

This workshop about Artificial Intelligence (AI) and its weakness to ‘Adversarial Attacks’ will tackle ways AI vision can be misled by adding visual noise to images, leading to malfunctions in the system. Javier will give an introduction to Adversarial Attacks applied to images and we will explore code examples that implement different adversarial techniques. This workshop will also propose a space for dialogue and discussion around the subject.

The project is subsidized by the Dutch funds 'Stimuleringsfonds Creatieve Industrie' for Digital Culture. 

## Constraints
The workshop is limited to around 8 active participants. Mail to contact@hsbxl.be if you want to join.

Participants need to bring their own laptop. In some code examples we will use the webcams from your laptops. In case of having also a USB webcam, bring it with you too (optional). 

Although we will tinker with code examples (cloud-based Python + PyTorch), coding skills are not required for this workshop.


