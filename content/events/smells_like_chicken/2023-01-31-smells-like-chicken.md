---
startdate:  2023-01-31
starttime: "18:00"
endtime: "19:00"
linktitle: "If it smells like chicken, you're holding it wrong"
title: "Basic soldering workshop: The basics"
price: "pay €6 for the soldering kit"
image: "beetle.png"
eventtype: "Workshop"
location: "HSBXL"
series: "byteweek2023"
---

## Basic concepts of soldering

At the end of the workshop, you can take home your self soldered blinking beetle and learned a few simple concepts around electronics and soldering along the way.