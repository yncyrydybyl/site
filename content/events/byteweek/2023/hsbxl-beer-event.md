---
startdate:  2023-02-03
starttime: "19:00"
enddate:  2023-02-03
endtime: "23:00"
allday: true
linktitle: "HSBXL beer tasting event"
title: "HSBXL beer tasting event"
location: "HSBXL"
eventtype: "beer tasting"
price: "Free"
series: "byteweek2023"
image: ""
--- 

## Free as in Freedom
Let's be clear here... Belgium has some great beers! As FOSDEM didn't have time anymore to organize a beer event, the hackerspace thought we could do something similar at our hackerspace. So *this event is not organized by or related to FOSDEM in any way* other than that we love the event and the people attending it.

We stocked up some beer - an assortment from smaller and bigger breweries. We'll be glad to let you discover some of the flavors. Friend of the house is microbrewery La Jungle. We'll be glad to be offering their beers too.

All beer will be in bottles but we'll start off with a wide variety so it's not practical to have all the glasses at hand. If you feel strong about having a glass, feel free to bring your own glass.