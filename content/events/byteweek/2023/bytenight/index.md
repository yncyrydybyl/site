---
startdate:  2023-02-04
starttime: "20:00"
linktitle: "Bytenight"
title: "Bytenight 2023: Finally again!"
location: "HSBXL"
eventtype: "Thé dansant"
price: "pay for your drinks please"
image: "bytenight.png"
series: "byteweek2023"
aliases: [/bytenight/]
---


As an after-party to the big FOSDEM Free Software conference in Brussels,  
the oldest Hackerspace in town organises its annual free party.

# Version 12 (0xc)
(space to be filled in)

# Music
Weird music for weird people.

## Lineup
TBA

# Drinks
* Beers on tap, specialty of [La Jungle](https://www.lajungle.be/)
* Collectable Bytenight 2023 cup

# Unsupported, deprecated versions
- [Bytenight v2020](https://hsbxl.be/events/byteweek/2020/bytenight/)
- [Bytenight v2019](https://hsbxl.be/events/byteweek/2019/bytenight/)
- [Bytenight v2018](https://wiki.hsbxl.be/Bytenight_2018)
- [Bytenight v2017](https://wiki.hsbxl.be/Bytenight_2017)
- [Bytenight v2016](https://wiki.hsbxl.be/Bytenight_(2016))
- [Bytenight v2015](https://wiki.hsbxl.be/Bytenight_(2015))
- [Bytenight v2014](https://wiki.hsbxl.be/Bytenight_(2014))
- [Bytenight v2013](https://wiki.hsbxl.be/Bytenight_2013)
- [Bytenight v2012](https://wiki.hsbxl.be/ByteNight_(2012))
- [Bytenight v2011](https://wiki.hsbxl.be/ByteNight_(2011))
- [Bytenight v2010](https://wiki.hsbxl.be/ByteNight_(2010))


# Organizing
Notes can be found on https://etherpad.openstack.org/p/bytenight2020