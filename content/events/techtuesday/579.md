---
techtuenr: "579"
startdate: 2020-07-28
starttime: 19:00
price: ""
eventtype: Social get-together
eventid: techtue579
series: TechTuesday
title: TechTuesday 579
linktitle: TechTue 579
location: HSBXL
image: techtuesday.png
---

Techtuesdays are a social meet-up sort of thing, just walk in for a talk, club-mate, latest news, or to show off your latest pet-project ... or you can always stay home and read slashdot.
