---
techtuenr: "664"
startdate: 2023-01-17
starttime: 19:00
price: ""
eventtype: Social get-together
eventid: techtue664
series: TechTuesday
title: TechTuesday 664
linktitle: "TechTue 664"
location: HSBXL
image: techtuesday.png
---
Techtuesdays are a social meet-up sort of thing, just walk in for a talk, club-mate, latest news, or to show off your latest pet-project ... or you can always stay home and read slashdot.
