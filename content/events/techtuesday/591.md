---
techtuenr: "591"
startdate: 2021-08-24
starttime: 19:00
price: ""
eventtype: Social get-together
eventid: techtue591
series: TechTuesday
title: TechTuesday 591
linktitle: "TechTue 591"
location: HSBXL
image: techtuesday.png
---
Techtuesdays are a social meet-up sort of thing, just walk in for a talk, club-mate, latest news, or to show off your latest pet-project ... or you can always stay home and read slashdot.
