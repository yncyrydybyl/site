---
techtuenr: "621"
startdate: 2022-03-22
starttime: 19:00
price: ""
eventtype: Social get-together
eventid: techtue621
series: TechTuesday
title: TechTuesday 621
linktitle: "TechTue 621"
location: HSBXL
image: techtuesday.png
---
Techtuesdays are a social meet-up sort of thing, just walk in for a talk, club-mate, latest news, or to show off your latest pet-project ... or you can always stay home and read slashdot.
