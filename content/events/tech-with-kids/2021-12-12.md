---
startdate:  2021-12-12
starttime: "14:30"
endtime: "17:00"
linktitle: "Tech with kids - Virtual Reality"
title: "Tech with kids - Virtual Reality (NL-FR-EN-...)"
price: "Free"
image: "tech_with_kids"
series: "Tech With Kids"
eventtype: "for kids age 6+ with parents"
location: "HSBXL"
---

# Canceled due to COVID-19 measures. 
New date set to March 20th.
