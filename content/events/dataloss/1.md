---
startdate:  2019-05-05
starttime: "13:00"
linktitle: "Dataloss anonymous [CANCELED]"
title: "Dataloss anonymous 1"
location: "HSBXL"
eventtype: "Self-help workshop"
price: ""
series: "dataloss"
image: "supportgroup.jpg"
---
    
    A bi-monthly "Dataloss anonymous" support group.<br>
    Find closing on your data loss grief.<br>
    We help each other preventing losing data again.<br>
    
!! CANCELED !!

As we all know, there are  [5 stages in grief](https://grief.com/the-five-stages-of-grief).  
Anyone who has suffered (data) loss, knows these phases.
 
1. **Denial** - I didn't suffer any data loss, I'll for sure find some cloud that had my data...
2. **Anger** - It's all the fault of others! Stupid hard disk! Crappy laptop! Evil crook!
3. **Bargaining** - Just sent a personal mail to the NSA, please can you pull up my data?
4. **Depression** - It's all lost, you're ready to give up and never touch computers again in your life...
5. **Acceptance** - Accepting the pain of loss, allowing it to free up space on your hard disk to engage in new projects...


# Agenda
Every workshop we'll pick a backup solution to investigate.  
List is maintained on the [dataloss anonymous](/events/dataloss) landingpage.

# Links
- tbd.
